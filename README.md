# Investbro #

This README explains the foundations of Investbro app. 

## Business ##

Investbro is a web app that aims stock exchange investors which wants to find and matches to other investor partners to investment interests and/or work together. The app offers a easy random profile recommendation based on near location of user and an interactive chat list.     

## Database ##

This section give us a visual percepetion of main database entities and their relationships.

<img src="./img/investbro-database.png" style="border-radius: 8px;"/>

## Assets  ##

This app operations is distributed through these assets below.

<img src="./img/investbro-assets.png" style="border-radius: 8px;"/>

## Architecture  ##

All the assets, excepts database, are based in Clean Architecture, following the Dependency Rule: <i>"source code dependencies can only point inwards"<i>.

<img src="https://blog.cleancoder.com/uncle-bob/images/2012-08-13-the-clean-architecture/CleanArchitecture.jpg" style="border-radius: 8px;" />



